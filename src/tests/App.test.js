import React from 'react';
import ReactDOM from 'react-dom';
import App from '../App';
import {
  mount
} from 'enzyme';
import {
  configure
} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({
  adapter: new Adapter()
});
describe('Meu react Teste', () => {

  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render( <App /> , div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('renders without crashing1 ', () => {
    const props = {
      value: '10.03.2018'
    };
    const AppComponent = mount(<App {...props} />);
    expect(AppComponent.prop('value')).toEqual('10.03.2018');
  });
  
});
