import FetchApi from './../library/fetch-api';
import PlanetModel from './../library/planet-model';
import Planet from './planet';
import React from 'react';
import StatusMsg from './status-msg';
export default class RandomPlanet extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      planet: {},
      status: ''
    };
    this.hasErrorPolling = process.env.REACT_APP_HAS_ERROR_POLLING;
    this.pollingTime = process.env.REACT_APP_ERROR_POLLING_TIME;
    this.PLANETS_URL=process.env.REACT_APP_PLANETS_URL;
  }
  componentDidMount() {
    this.getPlanet(new Event('init'));
  }
  hasPlanet() {
    return Object.keys(this.state.planet).length !== 0;
  }
  clearPlanet() {
    let state = this.state;
    state.planet = {};
    this.setState(state);
  }
  setMessage(statusText) {
    let state = this.state;
    state.status = statusText;
    this.setState(state);
  }
  isLoading() {
    return this.state.status === '' && !this.hasPlanet();
  }
  setTimeout(callback, time) {
    if (this.hasErrorPolling) {
      setTimeout(callback, this.pollingTime);
    }
  }
  getRemote(endpoint, category) {
    let url;
    switch (category) {
      case 'planets':
        url = `${this.PLANETS_URL}/${endpoint}`;
        break;
      default:
        url = `${endpoint}`;
    }
    return FetchApi.get(url);
  }
  getPlanet(event) {
    event.preventDefault();
    this.setMessage('');
    this.clearPlanet();
    return new Promise(async (resolve, reject) => {
      try {
        let planets = await this.getRemote('', 'planets');
        let randomPlanetId = Math.floor(Math.random() * (planets.count - 1)) + 1;
        let planet = await this.getRemote(randomPlanetId, 'planets');
        if (planet) {
            planet.films = await this.getFilms(planet.films);
          
          this.setState({ planet: new PlanetModel(planet) });
          resolve(true);
        }
      } catch (error) {
        this.setMessage(`Error: ${error.message}`);
        this.clearPlanet();
        this.setTimeout(() => this.getPlanet(new Event('callbackError')));
        resolve(true);
      }
    })
  }
  getFilms(films) {
    let filmsList = [];
    return new Promise(async (resolve, reject) => {
      films.forEach(async film => {
        filmsList.push(this.getRemote(film, 'films'));
      });
      Promise.all(filmsList).then(filmListResolved => resolve(filmListResolved));
    });
  }
  render() {
    return (
      <div className="planet-wrapper">
        <div className="planet-content">
          {(() => {
            if (this.hasPlanet()) {
              return (<Planet planet={this.state.planet} />)
            } else {
              return (<StatusMsg status={this.state.status} />)
            }
          })()}
        </div>
        <div className="planet-next">
          <button disabled={this.isLoading()} onClick={this.getPlanet.bind(this)}>NEXT</button>
        </div>
      </div>
    );
  }
}

