import React from 'react';
import PlanetModel from './../library/planet-model';
export default class Planet extends React.Component {
  constructor(props) {
    super(props);
    this.planet = props.planet;
    this.state ={
      popup: ''
    };
  }
  componentWillReceiveProps(nextProps) {
    if (this.props !== nextProps) {
      this.planet = new PlanetModel(nextProps.planet);
    }
  }
  getTitle(v) {
    return `
    TITLE: ${v.title}\nEPISODE: ${v.episode_id}\nPRODUCER: ${v.producer}\nDATE RELEASE: ${v.release_date}\nDIRECTOR: ${v.director}\nSINOPSE: ${v.opening_crawl}
    `;
  }
  setPopup(data) {
    this.setState({popup: data})
  }
  getPopupContent(){
    return this.state.popup;
  }
  clearPopup(event) {
    this.setState({popup: ''})
  }
  render() {
    return (
      <div className="content">
        <div className="title">{this.planet.name}</div>
        <div className="info">
          <div><span>POPULATION: </span>{this.planet.population}</div>
          <div><span>CLIMATE: </span>{this.planet.climate}</div>
          <div><span>TERRAIN: </span>{this.planet.terrain}</div>
          <div className="feature"><span>FEATURE IN </span>
            {(() => {
              if (this.planet.films)
                switch (this.planet.films.length) {
                  case 0: return <div>{"N/A"}</div>;
                  default: return this.planet.films.map((v, i) => <div onClick={this.setPopup.bind(this, this.getTitle(v))} key={'film'+i}>{v.title}({v.release_date.substr(0,4)})</div>);
                }
            })()}
          </div>
        </div>
        <div className={this.state.popup !=='' ? "popup-overlay visible" : "popup-overlay"}>
          <div className="popup">
            <div className="close" onClick={this.clearPopup.bind(this)}>&times;</div>
            <div className="content">
              {this.getPopupContent()}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
