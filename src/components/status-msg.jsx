import React from 'react';
export default class StatusMsg extends React.Component {
  constructor(props) {
    super(props);
    this.status = props.status;
  }
  componentWillReceiveProps(nextProps) {
    if (this.props !== nextProps) {
      this.status = nextProps.status
    }
  }
  render() {
    return (
      (() => {
        switch (this.status) {
          case '':
            return (<div className="content-status"><div className="loader"></div>Loading...</div>)
          default:
            return (<div className="content-error"><div className="error">{this.status}</div></div>)
        }
      })()
    )
  }
}
