export default class FetchApi {
  static get(url) {
    return new Promise((resolve, reject) => {
      fetch(
          `${url}`, {
            method: 'get',
          })
        .then((response) => (response.ok ? response.json() : reject(new Error(response.statusText))))
        .then((data) => resolve(data))
        .catch((error) => reject(error.message));
    });
  }
}