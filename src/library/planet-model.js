export default class PlanetModel {
  constructor(json) {
    this.name = json.name ? json.name : '-';
    this.population = json.population ? json.population : 0;
    this.climate = json.climate ? this.formatDelimitator(json.climate, 'and') : '';
    this.terrain = json.terrain ? this.formatDelimitator(json.terrain, 'and') : '-';
    this.films = json.films ? json.films : [];
  }
  formatDelimitator(text, delimitator) {
    return text ? text.replace(/,\s([^,]+)$/, ` ${delimitator} $1`) : '';
  }
}
