import React from 'react';
import RandomPlanet from './components/random-planet';
class App extends React.Component {
  render() {
    return (
      <div className="App">
        <RandomPlanet />
      </div>
    );
  }
}

export default App;
